export const environment = {
  production: true,
  urlAuthSpring: 'https://api.thebutler.com.br/oauth',
  urlSpring: 'https://api.thebutler.com.br/api'
};
